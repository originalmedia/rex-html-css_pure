<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Bootswatch: Flatly</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" href="assets/css/main.css" media="screen">


    <style type="text/css">
      
      body > h1 {margin: 50px 10px;}

    </style>


    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/custom.js"></script>
    <script src="assets/js/rex-common.js"></script>
 
    
  </head>
  <body>



      <h1>Headers</h1>
    
      <?php include("_modules/organism/header/header.php"); ?>
      <?php include("_modules/organism/header/header_2.php"); ?>
      <?php include("_modules/organism/header/header_3.php"); ?>
      <?php include("_modules/organism/header/header_4.php"); ?>




      <h1>Institucional</h1>

      <?php include("_modules/templates/institutional/template-institutional.php"); ?>


      <h1>Nossas Lojas</h1>

      <?php include("_modules/templates/institutional/template-shops.php"); ?>

      <h1>Nossas Lojas - Map</h1>

      <?php include("_modules/templates/institutional/template-shops-map.php"); ?>


      <h1>Contato</h1>

      <?php include("_modules/templates/institutional/template-contact.php"); ?>

      <h1>Error</h1>

      <?php include("_modules/templates/template-error.php"); ?>


      <h1>Catálogo</h1>

      <?php include("_modules/templates/template-search.php"); ?>

<div style="display: none;">
      <h1>Mix and Match</h1>

      <?php include("_modules/templates/template-mix-and-match.php"); ?>

</div>
      <h1>Produto</h1>
      <h3>Produto - imagem</h3>

      <div class="container">
        <div class="row">
          <div class="col-sm-4 col-xs-12">
            <?php include("_modules/molecules/_product-image-group.php"); ?>
          </div>
        </div>
      </div>

      <h3>Produto - imagem</h3>

      <div class="container">
        <div class="row">
          <div class="col-sm-4 col-xs-12">
            <?php include("_modules/molecules/_product-info.php"); ?>
          </div>
        </div>
      </div>


      <h3>Produto - Template</h3>

      <?php include("_modules/templates/template-product.php"); ?>
      

      



    
  </body>
</html>
