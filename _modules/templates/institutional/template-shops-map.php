
<div class="container">

  <div class="row">
      <div class="col-md-3 col-xs-12">
          <?php include './_modules/molecules/_sidebar-links.php'; ?>
      </div>
      <div class="col-md-9 col-xs-12">

          <div class="rex-content-institutional-txt">
            <h1>Nossas Lojas</h1>
            
            <div class="row">

              <div class="col-sm-4 col-xs-8">
                <div class="form-group">
                  <label class="control-label" for="shop-address">Escolha uma loja</label>

                  <select id="shop-address">
                      <option value="">Selecione uma loja</option>
                      <option value="plaza-niteroi">Plaza Niterói</option>
                      <option value="downtown">Downtown</option>
                      <option value="ipanema">Ipanema</option>
                      <option value="shopping-da-gavea">Shopping da Gávea</option>
                      <option value="norte-shopping">Norte Shopping</option>
                      <option value="nova-america">Nova América</option>
                      <option value="centro">Centro</option>
                      <option value="barrashopping">BarraShopping</option>
                      <option value="icarai">Icaraí</option>
                      <option value="botafogo">Botafogo</option>
                      <option value="rio-design-barra">Rio Design Barra</option>
                      <option value="rio-sul">Rio Sul</option>
                      <option value="shopping-tijuca">Shopping Tijuca</option>
                      <option value="iguatemi">Iguatemi</option>
                      <option value="copacabana">Copacabana</option>
                      <option value="shopping-metropolitano">Shopping Metropolitano</option>
                  </select>
                </div>
              </div>

              <div class="col-sm-4 hidden-xs">
                <div class="form-group">
                  <label class="control-label" for="inpt-address">Digite seu endereço</label>
                  <input type="text" id="inpt-address">
                </div>
              </div>

              <div class="col-sm-4 col-xs-4">
                <div class="form-group">
                  <div class="rex-search-shops-button">
                    <a href="#" class="btn btn-success btn-block hidden-xs" id="rex-track-route">Traçar rota</a>

                    <a href="#" class="btn btn-success btn-block visible-xs" id="rex-track-route-mobile">Abrir no mapa</a>
                  </div>
                </div>
              </div>

            </div>  


            <div class="rex-map-content hidden-xs">

              <div class="rex-result-store">        
                <div class="store plaza-niteroi" data-lat="-22.8969447" data-lng="-43.12387309999997" style="display: none;">
                    <h2>Plaza Niterói</h2>
                    <p class="addressStore">
                        <span>Rua XV de Novembro, 8 Lojas 336/ 337, Centro - Niterói - RJ</span>
                    </p>
                    <a href="tel: 2613-5319">2613-5319</a><p></p>
                          
                </div>
            
                <div class="store downtown" data-lat="-22.9998142" data-lng="-43.34965090000003" style="display: none;">
                    <h2>Downtown</h2>
                    <p class="addressStore">
                        <span>Av. das Américas, 500 Bloco 07 | Lojas 109/ 110, Barra da Tijuca - Rio de Janeiro - RJ</span>
                    </p>
                    <a href="tel: 2491-2982">2491-2982</a><p></p>
                          
                </div>
            
                <div class="store ipanema" data-lat="-22.9839441" data-lng="-43.20884319999999" style="display: none;">
                    <h2>Ipanema</h2>
                    <p class="addressStore">
                        <span>Rua Visconde de Pirajá, 468 , Ipanema - Rio de Janeiro - RJ</span>
                    </p>
                    <a href="tel: 2513-4547">2513-4547</a><p></p>
                          
                </div>
            
                <div class="store shopping-da-gavea" data-lat="-22.9754557" data-lng="-43.228221899999994" style="display: none;">
                    <h2>Shopping da Gávea</h2>
                    <p class="addressStore">
                        <span>Rua Marquês de São Vicente, 52 Loja 232, Gávea - Rio de Janeiro - RJ</span>
                    </p>
                    <a href="tel: 2529-8022">2529-8022</a><p></p>
                           
                </div>
            
                <div class="store norte-shopping" data-lat="-22.8869273" data-lng="-43.28065090000001" style="display: none;">
                    <h2>Norte Shopping</h2>
                    <p class="addressStore">
                        <span>Av. Dom Helder Camara, 5080 Lojas 4201/ 4202, Cachambi - Rio de Janeiro - RJ</span>
                    </p>
                    <a href="tel: 2229-8875">2229-8875</a><p></p>
                           
                </div>
            
                <div class="store nova-america" data-lat="-22.8786873" data-lng="-43.271172699999966" style="display: none;">
                    <h2>Nova América</h2>
                    <p class="addressStore">
                        <span>Av. Pastor Martin Luther King, 126 Bloco 01 | Loja 103, Del Castilho - Rio de Janeiro - RJ</span>
                    </p>
                    <a href="tel: 2583-1018">2583-1018</a><p></p>
                           
                </div>
            
                <div class="store centro" data-lat="-22.9031917" data-lng="-43.17695249999997" style="display: none;">
                    <h2>Centro</h2>
                    <p class="addressStore">
                        <span>Rua da Quitanda, 74 , Centro - Rio de Janeiro - RJ</span>
                    </p>
                    <a href="tel: 2222-6991">2222-6991</a><p></p>
                          
                </div>
            
                <div class="store barrashopping" data-lat="-22.9976076" data-lng="-43.35819349999997" style="display: none;">
                    <h2>BarraShopping</h2>
                    <p class="addressStore">
                        <span>Av. das Américas, 4666 Lojas 237 | 1º piso, Barra da Tijuca - Rio de Janeiro - RJ</span>
                    </p>
                    <a href="tel: 3328-2177">3328-2177</a><p></p>
                          
                </div>
            
                <div class="store icarai" data-lat="-22.9083241" data-lng="-43.108986800000025" style="display: none;">
                    <h2>Icaraí</h2>
                    <p class="addressStore">
                        <span>Rua Engenheiro Guilherme Greenhalgh, 16 Lojas 101/ 102, Icaraí - Niterói - RJ</span>
                    </p>
                    <a href="tel: 2610-1843">2610-1843</a><p></p>
                           
                </div>
            
                <div class="store botafogo" data-lat="-22.9474751" data-lng="-43.1834197" style="display: none;">
                    <h2>Botafogo</h2>
                    <p class="addressStore">
                        <span>Praia de Botafogo, 400 Lojas 334/ 335, Botafogo - Rio de Janeiro - RJ</span>
                    </p>
                    <a href="tel: 2237-9119">2237-9119</a><p></p>
                        
                </div>
            
                <div class="store rio-design-barra" data-lat="-23.0014227" data-lng="-43.38589939999997" style="display: none;">
                    <h2>Rio Design Barra</h2>
                    <p class="addressStore">
                        <span>Av. das Américas, 7777 Loja 109, Barra da Tijuca - Rio de Janeiro - RJ</span>
                    </p>
                    <a href="tel: 3326-4291">3326-4291</a><p></p>
                         
                </div>
            
                <div class="store rio-sul" data-lat="-22.9571438" data-lng="-43.17672329999999" style="display: none;">
                    <h2>Rio Sul</h2>
                    <p class="addressStore">
                        <span>Rua Lauro Sodré, 445 Loja 201, Botafogo - Rio de Janeiro - RJ</span>
                    </p>
                    <a href="tel: 2275-6688">2275-6688</a><p></p>
                           
                </div>
            
                <div class="store shopping-tijuca" data-lat="-22.9219847" data-lng="-43.23565259999998" style="display: none;">
                    <h2>Shopping Tijuca</h2>
                    <p class="addressStore">
                        <span>Av. Maracanã, 987 Loja 1020, Tijuca - Rio de Janeiro - RJ</span>
                    </p>
                    <a href="tel: 2567-0966">2567-0966</a><p></p>
                           
                </div>
            
                <div class="store iguatemi" data-lat="-22.9192581" data-lng="-43.25017109999999" style="display: none;">
                    <h2>Iguatemi</h2>
                    <p class="addressStore">
                        <span>Rua Barão de São Francisco, 236 Loja 1083, Andaraí - Rio de Janeiro - RJ</span>
                    </p>
                    <a href="tel: 97458-4032">97458-4032</a><p></p>
                          
                </div>
            
                <div class="store copacabana" data-lat="-22.9719129" data-lng="-43.186575500000004" style="display: none;">
                    <h2>Copacabana</h2>
                    <p class="addressStore">
                        <span>Rua Santa Clara, 42 Loja A, Copacabana - Rio de Janeiro - RJ</span>
                    </p>
                    <a href="tel: 2549-0723">2549-0723</a><p></p>
                          
                </div>
            
                <div class="store shopping-metropolitano active" data-lat="-22.973021" data-lng="-43.37293699999998" style="display: block;">
                    <h2>Shopping Metropolitano</h2>
                    <p class="addressStore">
                        <span>Av. Embaixador Abelardo Bueno, 1300 Loja 2025 | 2° piso, Barra da Tijuca - Rio de Janeiro - RJ</span>
                    </p>
                    <a href="tel: 2421-1792">2421-1792</a><p></p>
                           
                </div>
              </div>
              <div id="map-canvas"></div>            


              <div id="rex-map-instructions">
                  <h3>Instruções</h3>
                  <div class="content">

                  </div>
              </div>
            </div>
           

            
          </div>

      </div>
  </div>

</div>


<script type="text/javascript">
    var img = "//www.armadillo.com.br/arquivos/pin-maps-armadillo.png";


  
    var mapGeocoding = function() {
   
      var map = new GMaps({
        div: '#map-canvas',
        lat: -22.905548,
        lng: -43.177235,
        mapTypeControl: false,
        panControl: false,
        zoomControl: false,
        icon: img
      });

        map.addMarker({
          lat: -22.8969447,
          lng: -43.12387309999997,
          icon: img,
          animation: google.maps.Animation.DROP
        });

      
        
        map.addMarker({
          lat: -22.9998142,
          lng: -43.34965090000003,
          icon: img,
          animation: google.maps.Animation.DROP
        });

      
        
        map.addMarker({
          lat: -22.9839441,
          lng: -43.20884319999999,
          icon: img,
          animation: google.maps.Animation.DROP
        });

      
        
        map.addMarker({
          lat: -22.9754557,
          lng: -43.228221899999994,
          icon: img,
          animation: google.maps.Animation.DROP
        });

      
        
        map.addMarker({
          lat: -22.8869273,
          lng: -43.28065090000001,
          icon: img,
          animation: google.maps.Animation.DROP
        });

      
        
        map.addMarker({
          lat: -22.8786873,
          lng: -43.271172699999966,
          icon: img,
          animation: google.maps.Animation.DROP
        });

      
        
        map.addMarker({
          lat: -22.9031917,
          lng: -43.17695249999997,
          icon: img,
          animation: google.maps.Animation.DROP
        });

      
        
        map.addMarker({
          lat: -22.8217398,
          lng: -43.045899299999974,
          icon: img,
          animation: google.maps.Animation.DROP
        });

      
        
        map.addMarker({
          lat: -22.9976076,
          lng: -43.35819349999997,
          icon: img,
          animation: google.maps.Animation.DROP
        });

      
        
        map.addMarker({
          lat: -22.9083241,
          lng: -43.108986800000025,
          icon: img,
          animation: google.maps.Animation.DROP
        });

      
        
        map.addMarker({
          lat: -22.9474751,
          lng: -43.1834197,
          icon: img,
          animation: google.maps.Animation.DROP
        });

      
        
        map.addMarker({
          lat: -23.0014227,
          lng: -43.38589939999997,
          icon: img,
          animation: google.maps.Animation.DROP
        });

      
        
        map.addMarker({
          lat: -22.9571438,
          lng: -43.17672329999999,
          icon: img,
          animation: google.maps.Animation.DROP
        });

      
        
        map.addMarker({
          lat: -22.9219847,
          lng: -43.23565259999998,
          icon: img,
          animation: google.maps.Animation.DROP
        });

      
        
        map.addMarker({
          lat: -22.9192581,
          lng: -43.25017109999999,
          icon: img,
          animation: google.maps.Animation.DROP
        });

      
        
        map.addMarker({
          lat: -22.9719129,
          lng: -43.186575500000004,
          icon: img,
          animation: google.maps.Animation.DROP
        });

      
        
        map.addMarker({
          lat: -22.973021,
          lng: -43.37293699999998,
          icon: img,
          animation: google.maps.Animation.DROP
        });

      


      //var obj = JSON.parse(localizacao);
      

      $('#shop-address').on('change', function(e){
        e.preventDefault();
        $('.rex-result-store .store').hide();
        $('.rex-result-store .store').removeClass('active');
        var className = $(this).val();
        var dataLat = $('.'+className).data('lat');
        var dataLng = $('.'+className).data('lng');
        $('.'+className).show();
        $('.'+className).addClass('active');

        map.setCenter(dataLat, dataLng);

        $("#rex-track-route-mobile").attr("href","geo:"+dataLat+","+dataLng);

      });

      // $('#stateList').on('change', function(){
      //  var className = $(this).val();
      //  $('.'+className).show();
      // });


      var trackRoute = function() {
        var latDest = $('.store.active').data('lat');
        var lngDest = $('.store.active').data('lng');
        var destination = latDest+', '+lngDest;
        var startPoint = $.trim($('#inpt-address').val());

        $('#rex-map-instructions .content').html("");

        map.cleanRoute();

        GMaps.geocode({
            address: startPoint,
            callback: function(results, status) {
                      if (status == 'OK') {
                          latlng = results[0].geometry.location;
                          map.setZoom(12);
                          map.addMarker({
                              lat: latlng.lat(),
                              lng: latlng.lng(),
                          });
                map.travelRoute({
                  origin: [latlng.lat(),latlng.lng()],
                    destination: [latDest, lngDest],
                  travelMode: 'driving',
                  step: function(e){
                  $('#rex-map-instructions .content').append('<p>'+e.instructions+'</p>');
                  $('#rex-map-instructions .content p:eq('+e.step_number+')').delay(100*e.step_number).fadeIn(500, function(){
                    map.setCenter(e.end_location.lat(), e.end_location.lng());
                    map.drawPolyline({
                      path: e.path,
                      strokeColor: '#131540',
                      strokeOpacity: 0.6,
                      strokeWeight: 6
                      });
                  });
                }
              });
            }
            }
        });
      }

      $("#inpt-address").keypress(function(e){
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if(keycode == '13') {
          e.preventDefault();               
          trackRoute();
        }           
      });



      $('#rex-track-route').click(function(e){
        e.preventDefault();
        trackRoute(); 

      });
    }
   

$(document).ready(function() {
  mapGeocoding();
});

</script>

 <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="./assets/js/gmaps.js"></script>
