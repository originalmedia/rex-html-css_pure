
<div class="rex-product-info-wrap">

	<div class="rex-product-info"> <!--  -->

		<h1>Vestido Casual Xadrez</h1>
	    <div class="ref">REF.: 
	    	<!-- <vtex.cmc:productReference /> -->
	    	<div class="productReference 41184_CAMEL">41184_CAMEL</div>
	    </div>

	    <div class="rex-product-desc">                        
	        <!--  <vtex.cmc:productDescription /> -->
	        <div class="productDescription">Blusa de camurça estampa plié leopardo com patches de tigre e rosas na frente. Possui recorte a laser. <br><br>90% poliéster, 10% elastano.</div>
	    </div> 
	    
	    <div class="rex-sku-select">
	        <?php include './_modules/atoms/_sku-selection.php'; ?>
	    </div>                    
	    

	    <div class="rex-boxbuy">
	        <div class="rex-product-price">
	            <!--  <vtex.cmc:skuPrice /> -->
	            <div class="plugin-preco"><div class="productPrice"><p class="descricao-preco"><em class="valor-de price-list-price" style="display: inline-block;">De: <strong class="skuListPrice">R$ 149,90</strong></em><em class="valor-por price-best-price" style="display: block;">Por: <strong class="skuBestPrice">R$ 399,90</strong></em><em class="valor-dividido price-installments" style="display: block;"><span><span>ou <label class="skuBestInstallmentNumber">6<span class="x">x</span></label> de </span><strong><label class="skuBestInstallmentValue">R$ 66,65</label></strong></span></em></p><p class="preco-a-vista price-cash" style="display: block;">Preço a vista:<strong class="skuPrice">R$ 399,90</strong></p></div></div>
	        </div>
	        
	        <!-- ####
	        	Example with 2 columns and the field of quantify 

	        <div class="rex-product-buy-button-wrap with-field">
	        	<div class="row">
	        		<div class="col-xs-4">
			        	<div class="prod-quantify">
			        		<label>Qtde.</label>
			        		<div class="field">
			        			<input type="text" value="1" class="qtdValue" _flx1_12_1="1">
			        			<span class="add-item">+</span><span class="remove-item">-</span>
			        		</div>
			        	</div>
		        	</div>
		        	<div class="col-xs-8">
				        <div class="rex-product-buy-button">
				        </div>
				    </div>
		        </div>
	        </div>

	        -->

	        <div class="rex-product-buy-button-wrap">
		        <div class="rex-product-buy-button">                                
		            <?php include './_modules/atoms/_product-buy-button.php'; ?>
		        </div>
	        </div>

	    </div>

	    <div class="product-size-table-link">
	    	<a href="#" class="btn btn-link btn-sm" data-toggle="modal" data-target="#size-table">Tabela de medidas</a>
	    </div>
		
	</div>

	<div class="rex-share">
	    <span style="display: inline-block" class='st_facebook_large' displayText='Facebook'></span>
	    <span style="display: inline-block" class='st_twitter_large' displayText='Tweet'></span>
	    <span style="display: inline-block" class='st_googleplus_large' displayText='Google +'></span>
	    <span style="display: inline-block" class='st_pinterest_large' displayText='Pinterest'></span>
	</div> <!-- socialnetwork --> 

</div>


<!-- Modal -->
<div class="modal fade" id="size-table" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tamanhos e Medidas</h4>
      </div>
      <div class="modal-body">
        Tamanhos...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "6a29bd9f-67aa-4a43-976d-5e9f0b13efa7", doNotHash: true, doNotCopy: false, hashAddressBar: false});</script>


