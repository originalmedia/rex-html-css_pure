<!-- #### model 

<div id="cart">
    <a title="Carrinho de Compras" href="/checkout/">
        <i class="icon-bag"></i>
        <vtex.cmc:AmountItemsInCart />
    </a>
    <vtex.cmc:miniCart />
</div>

-->


<div id="cart">
<a title="Carrinho de Compras" href="/checkout/"><i class="icon-bag"></i><div class="portal-totalizers-ref"><div class="amount-items-in-cart">

<div class="cartInfoWrapper"><span class="title"><span id="MostraTextoXml1">Resumo do Carrinho</span></span><ul class="cart-info"><li class="amount-products"><strong><span id="MostraTextoXml2">Total de Produtos:</span></strong> <em class="amount-products-em">0</em></li><li class="amount-items"><strong><span id="MostraTextoXml3">Itens:</span></strong> <em class="amount-items-em">0</em></li><li class="amount-kits"><strong><span id="MostraTextoXml4">Total de Kits:</span></strong> <em class="amount-kits-em"></em></li><li class="total-cart"><strong><span id="MostraTextoXml5">Valor Total:</span></strong> <em class="total-cart-em">R$ 0,00</em></li></ul></div></div></div></a>

<div class="portal-minicart-ref"><div class="v2-vtexsc-cart vtexsc-cart mouseActivated preLoaded"><div class="vtexsc-bt"></div><div class="vtexsc-center"><div class="vtexsc-wrap "><table class="vtexsc-productList"><thead><tr><th class="cartSkuName" colspan="2">Produto</th><th class="cartSkuPrice">Preço</th><th class="cartSkuQuantity">Quantidade</th><th class="cartSkuActions">Excluir</th></tr></thead><tbody><tr><td class="cartSkuImage"><a class="sku-imagem" href="/jaqueta-de-camurca-com-estampa-plie-leopardo-com-b-41270_caramelo/p"><img height="71" width="71" alt="Jaqueta De Camurça Estampa Plié Leopardo Caramelo Bordado" src="https://lojacaos.vteximg.com.br/arquivos/ids/383631-210-210/41270_caramel_1.jpg"></a></td><td class="cartSkuName"><h4><a href="/jaqueta-de-camurca-com-estampa-plie-leopardo-com-b-41270_caramelo/p">Jaqueta De Camurça Estampa Plié Leopardo Caramelo Bordado</a></h4><p class="availability"></p></td><td class="cartSkuPrice"><div class="cartSkuUnitPrice"><span class="bestPrice">R$ 1.499,90</span></div></td><td class="cartSkuQuantity"><div class="cartSkuQtt"><span class="cartSkuQttTxt"><span class="vtexsc-skuQtt">1</span></span></div></td><td class="cartSkuActions"><span class="cartSkuRemove" data-index="0"><a href="javascript:void(0);" class="text" style="display: none;">excluir</a></span><div class="vtexsc-overlay" style="display: none;"></div></td></tr></tbody></table></div><div class="cartFooter clearfix"><div class="cartTotal">Total<span class="vtexsc-totalCart"><span class="vtexsc-text">R$ 1.499,90</span></span></div><a href="/checkout/#/orderform" class="cartCheckout"></a></div></div><div class="vtexsc-bb"></div></div></div>


</div>



