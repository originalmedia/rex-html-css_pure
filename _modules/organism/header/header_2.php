<div id="header" class="rex-header-v2">
  <nav class="navbar navbar-default">
    <div class="container">

      <div class="rex-wrapper">

          <div class="navbar-header">

            <div class="visible-xs cart-mobile">
              <?php include './_modules/atoms/_cart.php'; ?>
            </div>

            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>


            <div class="logo-wrap">
              <a class="logo" href="#"><img src="http://placehold.it/111x60"/></a>
            </div>

          </div>

          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">


            
            <form class="navbar-form navbar-left" role="search">
              <?php include './_modules/atoms/_search.php'; ?>
              
            </form>
            <ul class="nav navbar-nav navbar-right">
              <li><a href="#">Link</a></li>
              <li class="hidden-xs"><?php include './_modules/atoms/_cart.php'; ?></li>
            </ul>
          </div>
          

      </div>
    </div>

    <div class="menu">
      <div class="container">
        <ul class="nav navbar-nav">
          <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
          <li><a href="#">Link</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Action</a></li>
              <li><a href="#">Another action</a></li>
              <li><a href="#">Something else here</a></li>
              <li class="divider"></li>
              <li><a href="#">Separated link</a></li>
              <li class="divider"></li>
              <li><a href="#">One more separated link</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>


  </nav>
</div>