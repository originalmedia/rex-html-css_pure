<div id="header" class="rex-header-v3">
  <nav class="navbar navbar-default">
    <div class="container">

      <div class="rex-wrapper">
        <div class="row">


          <div class="col-md-4 visible-md visible-lg">
            <div class="welcomeMessage">
                <?php include './_modules/atoms/_welcome-message.php' ?>
            </div>
          </div>


          <div class="col-md-4 col-xs-12 ">
              <div class="logo-wrap">
                <a class="logo" href="#"><img src="http://placehold.it/200x60"/></a>
              </div>
          </div>

         

          <div class="col-md-4 col-xs-12 collapse search-wrapper" id="bs-search-collapse">
            <form class="search-form" role="search">
              <?php include './_modules/atoms/_search.php'; ?> 
            </form>
          </div>

        </div>
      </div>

      
    </div>

    <div class="menu"> 
      <div class="container">
        <ul class="nav navbar-nav">
          <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
          <li><a href="#">Link</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Action</a></li>
              <li><a href="#">Another action</a></li>
              <li><a href="#">Something else here</a></li>
              <li class="divider"></li>
              <li><a href="#">Separated link</a></li>
              <li class="divider"></li>
              <li><a href="#">One more separated link</a></li>
            </ul>
          </li>
        </ul>
        <ul class="nav navbar-nav shop-links pull-right">
          <li class="visible-sm visible-xs collapsed" data-toggle="collapse" data-target="#bs-search-collapse"><i class="icon-search"></i></li>
          <li><?php include './_modules/atoms/_cart.php'; ?></li>
        </ul>
      </div>
    </div>


  </nav>
</div>  