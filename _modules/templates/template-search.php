
<div class="container">

  <div class="rex-breadcrumb">
    <?php include './_modules/molecules/_breadcrumb.php'; ?>
  </div>

  <div class="row">
      <div class="col-md-3 col-xs-12">

          <div class="rex-search-filters">
            <?php include './_modules/molecules/_sidebar-filters.php'; ?>
          </div>
      </div>
      <div class="col-md-9 col-xs-12">

          <div class="rex-shelf">

              <?php include './_modules/organism/_shelf.php'; ?>

          </div>

      </div>
  </div>

</div>
