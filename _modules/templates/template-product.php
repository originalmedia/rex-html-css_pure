<div class="container">
   <div class="row">
     <div class="col-sm-7 col-xs-12">
       <?php include("./_modules/molecules/_product-image-group.php"); ?>
     </div>
     <div class="col-sm-4 col-sm-offset-1 col-xs-12">
       <?php include("./_modules/molecules/_product-info.php"); ?>
     </div>
   </div>
 </div>