
<div class="container">

  <div class="row">
      <div class="col-md-3 col-xs-12">
          <?php include './_modules/molecules/_sidebar-links.php'; ?>
      </div>
      <div class="col-md-9 col-xs-12">

          <div class="rex-content-institutional-txt">
            <h1>Nossas Lojas</h1>

            <div class="rex-box-banner">
                <img src="http://placehold.it/950x230"/>
            </div>

            <div class="row">
                <div class="col-xs-6">
                  <div class="list-group rex-shops-list">
                    
                    <div class="list-group-item">
                      <h4 class="list-group-item-heading"><strong>Botafogo</strong></h4>
                      <div class="list-group-item-text">
                          <address>Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</address>
                          <a href="tel:18475555555">1-847-555-5555</a>

                      </div>
                    </div>

                    <div class="list-group-item">
                      <h4 class="list-group-item-heading"><strong>Botafogo</strong></h4>
                      <div class="list-group-item-text">
                          <address>Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</address>
                          <a href="tel:18475555555">1-847-555-5555</a>

                      </div>
                    </div>

                    <div class="list-group-item">
                      <h4 class="list-group-item-heading"><strong>Botafogo</strong></h4>
                      <div class="list-group-item-text">
                          <address>Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</address>
                          <a href="tel:18475555555">1-847-555-5555</a>

                      </div>
                    </div>

                   
                  </div>
                </div>

                <div class="col-xs-6">
                  <div class="list-group rex-shops-list">
                    
                    <div class="list-group-item">
                      <h4 class="list-group-item-heading"><strong>Botafogo</strong></h4>
                      <div class="list-group-item-text">
                          <address>Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</address>
                          <a href="tel:18475555555">1-847-555-5555</a>

                      </div>
                    </div>

                    <div class="list-group-item">
                      <h4 class="list-group-item-heading"><strong>Botafogo</strong></h4>
                      <div class="list-group-item-text">
                          <address>Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</address>
                          <a href="tel:18475555555">1-847-555-5555</a>

                      </div>
                    </div>

                    <div class="list-group-item">
                      <h4 class="list-group-item-heading"><strong>Botafogo</strong></h4>
                      <div class="list-group-item-text">
                          <address>Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</address>
                          <a href="tel:18475555555">1-847-555-5555</a>

                      </div>
                    </div>

                   
                  </div>
                </div>

            </div>

            
           

            
          </div>

      </div>
  </div>

</div>
